
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', name: 'home', component: () => import('pages/Index.vue') },
      { path: '/Auth', name: 'Auth', component: () => import('pages/Auth.vue'),
      children : [
        { path: '/login', name:'login'},
        { path: '/register', name:'register'},
        { path: '/forget', name:'forget'},
      ]
      },
      { name:'Dashboard', path: '/Dashboard', component: () => import('pages/Dashboard.vue') },
      { name:'Courses', path: '/Courses', component: () => import('pages/Courses.vue') },
      { name:'singleCourse', path: 'Course/:slug', component: () => import('pages/Courses.vue'), props: true, },
      { name:'scripts', path: '/scripts', component: () => import('pages/scripts.vue') },
      { name:'classroom', path: '/classroom', component: () => import('pages/Courses.vue'), props: true, },
      { name:'settings', path: '/settings', component: () => import('pages/scripts.vue') },
      { name:'profile', path: '/profile', component: () => import('pages/Index.vue') },
      { name:'notification', path: '/notification', component: () => import('pages/Index.vue') },
      { name:'staffroom', path: '/staffroom', component: () => import('pages/Index.vue'),
        children : [
          { path: '/scripts', name:'scripts'},
          { path: '/lecture_note', name:'lecture_note'},
          { path: '/assignment', name:'assignment'},
          { path: '/curiculum', name:'curiculum'},
        ]
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
